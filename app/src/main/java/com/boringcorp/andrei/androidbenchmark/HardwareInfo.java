package com.boringcorp.andrei.androidbenchmark;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Pair;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HardwareInfo extends AppCompatActivity {

    private Map<Integer, Pair<Long, Long>> minMaxFreqMap = new HashMap<>();
    private Map<Integer, Pair<String, String>> listGPULiveData = new HashMap<>();
    private TextView cpuInfoTextView;
    private TextView gpuInfoTextView;

    private String details = "VERSION.RELEASE : " + Build.VERSION.RELEASE
            + "\nVERSION.INCREMENTAL : " + Build.VERSION.INCREMENTAL
            + "\nVERSION.SDK.NUMBER : " + Build.VERSION.SDK_INT
            + "\nBOARD : " + Build.BOARD
            + "\nBOOTLOADER : " + Build.BOOTLOADER
            + "\nBRAND : " + Build.BRAND
            + "\nCPU_ABI : " + Build.CPU_ABI
            + "\nDISPLAY : " + Build.DISPLAY
            + "\nFINGERPRINT : " + Build.FINGERPRINT
            + "\nHARDWARE : " + Build.HARDWARE
            + "\nHOST : " + Build.HOST
            + "\nID : " + Build.ID
            + "\nMANUFACTURER : " + Build.MANUFACTURER
            + "\nMODEL : " + Build.MODEL
            + "\nPRODUCT : " + Build.PRODUCT
            + "\nSERIAL : " + Build.SERIAL
            + "\nTAGS : " + Build.TAGS
            + "\nTIME : " + Build.TIME
            + "\nTYPE : " + Build.TYPE
            + "\nUSER : " + Build.USER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hardware_info);

        cpuInfoTextView = findViewById(R.id.hardware_info_string);
        ArrayList<Pair<String, String>> coresList = getCoresInfo();

        for (Pair<String, String> core : coresList) {
            cpuInfoTextView.append(core.first + " : " + core.second + "\n");
        }

        gpuInfoTextView = findViewById(R.id.hardware_gpu_string);
        getGPUData();
        for (int i = 0; i < listGPULiveData.size(); i++) {
            gpuInfoTextView.append(listGPULiveData.get(i).first + " : " + listGPULiveData.get(i).second + "\n");
        }

        gpuInfoTextView.append(details);

    }

    public ArrayList<Pair<String, String>> getCoresInfo() {
        ArrayList<Pair<String, String>> coresList = new ArrayList<>();
        int numberOfCores = getNumberOfCores();

        coresList.add(new Pair<>("Cores", Integer.toString(numberOfCores)));
        for (int i = 0; i < numberOfCores; i++) {
            coresList.add(new Pair<>("     Core " + Integer.toString(i), getCurrentFrequency(i)));
        }

        return coresList;
    }

    public int getNumberOfCores() {
        return Runtime.getRuntime().availableProcessors();
    }

    public String getCurrentFrequency(int coreNumber) {
        String frequency = "Stopped";
        String path = "/sys/devices/system/cpu/cpu" + Integer.toString(coreNumber) + "/cpufreq/scaling_cur_freq";

        try {
            RandomAccessFile reader = new RandomAccessFile(path, "r");
            String string = reader.readLine();
            long value = Long.parseLong(string) / 1000;
            reader.close();
            frequency = Long.toString(value) + "MHz";

            if (minMaxFreqMap.containsKey(coreNumber)) {
                frequency += " (" + +minMaxFreqMap.get(coreNumber).first + "MHz - " + minMaxFreqMap.get(coreNumber).second + "MHz)";
            } else {
                Pair<Long, Long> minMaxPair = getMinMAxFreq(coreNumber);
                if (minMaxPair != null) {
                    frequency += " (" + minMaxPair.first + "MHz - " + minMaxPair.second + "MHz)";
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return frequency;
    }

    public Pair<Long, Long> getMinMAxFreq(int coreNumber) {
        String minPath = "/sys/devices/system/cpu/cpu" + Integer.toString(coreNumber) + "/cpufreq/cpuinfo_min_freq";
        String maxPath = "/sys/devices/system/cpu/cpu" + Integer.toString(coreNumber) + "/cpufreq/cpuinfo_max_freq";

        try {
            RandomAccessFile reader = new RandomAccessFile(minPath, "r");
            String string = reader.readLine();
            long minMhz = Long.parseLong(string) / 1000;
            reader.close();

            reader = new RandomAccessFile(maxPath, "r");
            string = reader.readLine();
            long maxMhz = Long.parseLong(string) / 1000;
            reader.close();

            Pair<Long, Long> values = new Pair<>(minMhz, maxMhz);
            minMaxFreqMap.put(coreNumber, values);
            return values;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void getGPUData() {
        if (listGPULiveData.isEmpty()) {
            ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
            String version = configurationInfo.getGlEsVersion();
            if (version != null || !version.isEmpty()) {
                String resource = getResources().getString(R.string.gles_version);
                Pair<String, String> resVers = new Pair<>(resource, version);
                listGPULiveData.put(0, resVers);
            }
        }
    }


}
