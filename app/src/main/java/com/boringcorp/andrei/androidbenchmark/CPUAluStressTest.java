package com.boringcorp.andrei.androidbenchmark;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class CPUAluStressTest extends AppCompatActivity {

    private Long size = Long.MAX_VALUE;
    private volatile boolean shouldTestRun;
    private long result;

    private TextView cpuAluInfo;
    private Spinner cpuAluMeasurement;
    private Button cpuAluTestStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpualu_stress_test);

        cpuAluTestStart = findViewById(R.id.startAluStressButton);
        cpuAluTestStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeTest();
                runTestwithTimer();
                stopTest();
                if(StopwatchTimeConverter.valueOf(cpuAluMeasurement.getSelectedItem().toString()).equals(StopwatchTimeConverter.Microsecond)){
                    cpuAluInfo.setText(stringResult(StopwatchTimeConverter.Microsecond));
                }else{
                    if(StopwatchTimeConverter.valueOf(cpuAluMeasurement.getSelectedItem().toString()).equals(StopwatchTimeConverter.Millisecond)){
                        cpuAluInfo.setText(stringResult(StopwatchTimeConverter.Millisecond));
                    }else {
                        cpuAluInfo.setText(stringResult(StopwatchTimeConverter.Seconds));
                    }
                }
            }
        });

        cpuAluInfo = findViewById(R.id.cpu_alu_info);
        cpuAluMeasurement = findViewById(R.id.cpu_alu_measurement);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.alu_stress_test_dropdown,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cpuAluMeasurement.setAdapter(adapter);

        cpuAluMeasurement.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                StopwatchTimeConverter selectedEnum = StopwatchTimeConverter.valueOf(selectedItemText);
                switch (selectedEnum){
                    case Microsecond: cpuAluInfo.setText(stringResult(StopwatchTimeConverter.Microsecond)); break;
                    case Millisecond: cpuAluInfo.setText(stringResult(StopwatchTimeConverter.Millisecond)); break;
                    case Seconds: cpuAluInfo.setText(stringResult(StopwatchTimeConverter.Seconds)); break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                cpuAluInfo.setText(R.string.alu_stress_dropdown_notsel);
            }
        });

    }

    public void initializeTest(){
        size = 20000000L;
        result = 0;
    }

    public void initializeTest(Long size){
        this.size = size;
        result = 0;
    }

    public void doAWarmUpTest(){
        Long prevSize = size;
        size = 1000L;
        for(int i = 1;i<3;i++){
            computeValues();
            size *= 3;
        }
        size = prevSize;
    }

    public void computeValues(){
        shouldTestRun = true;
        long aluResult = 1L;
        for(long i = 0L;i<size && shouldTestRun;i++){
            aluResult += i/256L;
            aluResult *= i % 3L + 1L;
            aluResult /= i % 2L + 1L;
        }
        shouldTestRun = false;
    }

    public void stopTest(){
        shouldTestRun = false;
    }

    public void runTestwithTimer(){
        final StopwatchTimer timer = new StopwatchTimer();
        doAWarmUpTest();
        timer.startStopwatch();
        computeValues();
        result = timer.stopStopwatch();
    }

    public String stringResult(StopwatchTimeConverter type){
        switch (type){
            case Millisecond: return "Arithmetic operations done in: " + StopwatchTimeConverter.convertTime(result,StopwatchTimeConverter.Millisecond) + "ms";
            case Microsecond: return "Arithmetic operations done in: " + StopwatchTimeConverter.convertTime(result,StopwatchTimeConverter.Microsecond) + "us";
            case Seconds: return "Arithmetic operations done in: " + StopwatchTimeConverter.convertTime(result,StopwatchTimeConverter.Seconds) + "s";
            default: return null;
        }
    }

}
