package com.boringcorp.andrei.androidbenchmark;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class FloatingPointStressTest extends AppCompatActivity {

    private Long size = Long.MAX_VALUE;
    private volatile boolean shouldTestRun;
    private long result;

    private Button startTest;
    private TextView fpuInfo;
    private Spinner fpuMeasurements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floating_point_stress_test);

        startTest = findViewById(R.id.fpu_start_test);
        startTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeTest();
                runTestwithTimer();
                stopTest();
                if(StopwatchTimeConverter.valueOf(fpuMeasurements.getSelectedItem().toString()).equals(StopwatchTimeConverter.Microsecond)){
                    fpuInfo.setText(stringResult(StopwatchTimeConverter.Microsecond));
                }else{
                    if(StopwatchTimeConverter.valueOf(fpuMeasurements.getSelectedItem().toString()).equals(StopwatchTimeConverter.Millisecond)){
                        fpuInfo.setText(stringResult(StopwatchTimeConverter.Millisecond));
                    }else {
                        fpuInfo.setText(stringResult(StopwatchTimeConverter.Seconds));
                    }
                }
            }
        });

        fpuInfo = findViewById(R.id.fpu_info);
        fpuMeasurements = findViewById(R.id.fpu_measurements);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.alu_stress_test_dropdown,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fpuMeasurements.setAdapter(adapter);

        fpuMeasurements.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                StopwatchTimeConverter selectedEnum = StopwatchTimeConverter.valueOf(selectedItemText);
                switch (selectedEnum){
                    case Microsecond: fpuInfo.setText(stringResult(StopwatchTimeConverter.Microsecond)); break;
                    case Millisecond: fpuInfo.setText(stringResult(StopwatchTimeConverter.Millisecond)); break;
                    case Seconds: fpuInfo.setText(stringResult(StopwatchTimeConverter.Seconds)); break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                fpuInfo.setText(R.string.alu_stress_dropdown_notsel);
            }
        });


    }

    public void initializeTest(){
        size = 20000000L;
        result = 0;
    }

    public void initializeTest(Long size){
        this.size = size;
        result = 0;
    }

    public void warmup() {
        Long prevSize = this.size;
        this.size = 1000L;
        for (int i=1; i<=3; i++){
            compute();
            this.size *= 10;
        }
        this.size = prevSize;
    }

    public void compute() {
        this.shouldTestRun = true;
        double sqrtPi = 1.0; // sqrt of PI
        double sqrtE = 1.0;  // sqrt of Euler's number
        double temp;
        for (double i = 0.0; i < this.size && this.shouldTestRun; i++){
            temp = sqrtPi + Math.PI / sqrtPi;
            sqrtPi = temp * 0.5;
            temp = sqrtE + Math.E / sqrtE;
            sqrtE = temp * 0.5;
        }
        this.shouldTestRun = false;
    }

    public void stopTest(){
        shouldTestRun = false;
    }

    public void runTestwithTimer() {
        final StopwatchTimer timer = new StopwatchTimer();
        this.warmup();
        timer.startStopwatch();
        this.compute();
        this.result = timer.stopStopwatch();
    }

    public String stringResult(StopwatchTimeConverter type){
        switch (type){
            case Millisecond: return "Arithmetic operations done in: " + StopwatchTimeConverter.convertTime(result,StopwatchTimeConverter.Millisecond) + "ms";
            case Microsecond: return "Arithmetic operations done in: " + StopwatchTimeConverter.convertTime(result,StopwatchTimeConverter.Microsecond) + "us";
            case Seconds: return "Arithmetic operations done in: " + StopwatchTimeConverter.convertTime(result,StopwatchTimeConverter.Seconds) + "s";
            default: return null;
        }
    }

}
