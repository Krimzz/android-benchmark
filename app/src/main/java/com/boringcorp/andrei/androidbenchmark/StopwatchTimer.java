package com.boringcorp.andrei.androidbenchmark;

public class StopwatchTimer {

    private long startDuration;
    private long duration;

    public void startStopwatch(){
        duration = 0;
        startDuration = System.nanoTime();
    }

    public long stopStopwatch(){
        long finalValue = duration + System.nanoTime() - startDuration;
        duration = 0;
        startDuration = 0;
        return finalValue;
    }

    public long pauseStopwatch(){
        duration += System.nanoTime() - startDuration;
        return duration;
    }

    public void resumeStopwatch(){
        startDuration = System.nanoTime();
    }
}
