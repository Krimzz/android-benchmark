package com.boringcorp.andrei.androidbenchmark;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button hardwareButton;
    private Button cpuAluStressTest;
    private Button cpuFPStressTest;
    private Button piDigitsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hardwareButton = findViewById(R.id.hardware_button);
        hardwareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HardwareInfo.class);
                startActivity(intent);
            }
        });

        cpuAluStressTest = findViewById(R.id.cpu_alu_button);
        cpuAluStressTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CPUAluStressTest.class);
                startActivity(intent);
            }
        });

        cpuFPStressTest = findViewById(R.id.cpu_fp_button);
        cpuFPStressTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FloatingPointStressTest.class);
                startActivity(intent);
            }
        });

        piDigitsButton = findViewById(R.id.pi_digits_button);
        piDigitsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PIDigitsCalculation.class);
                startActivity(intent);
            }
        });

    }
}
