package com.boringcorp.andrei.androidbenchmark;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.Bidi;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PIDigitsCalculation extends AppCompatActivity {

    public static final int THREAD_POOL_SIZE = 4;
    public static final int TOTAL_ITERATIONS = 5000;

    private volatile boolean shouldTestRun;
    private long result;
    private BigDecimal piResult = new BigDecimal(0);
    private int scale = 5000;
    private ArrayList<Future<BigDecimal>> chunksResult = new ArrayList<>();

    private Button startTest;
    private TextView piInfo;
    private Spinner piMeasurements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pidigits_calculation);

        startTest = findViewById(R.id.pi_start_button);
        startTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeTest();
                runTestWithTimer();
                stopTest();
                if(StopwatchTimeConverter.valueOf(piMeasurements.getSelectedItem().toString()).equals(StopwatchTimeConverter.Microsecond)){
                    piInfo.setText(stringResult(StopwatchTimeConverter.Microsecond));
                }else{
                    if(StopwatchTimeConverter.valueOf(piMeasurements.getSelectedItem().toString()).equals(StopwatchTimeConverter.Millisecond)){
                        piInfo.setText(stringResult(StopwatchTimeConverter.Millisecond));
                    }else {
                        piInfo.setText(stringResult(StopwatchTimeConverter.Seconds));
                    }
                }

            }
        });

        piInfo = findViewById(R.id.pi_info);
        piMeasurements = findViewById(R.id.pi_measurements);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.alu_stress_test_dropdown,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        piMeasurements.setAdapter(adapter);

        piMeasurements.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                StopwatchTimeConverter selectedEnum = StopwatchTimeConverter.valueOf(selectedItemText);
                switch (selectedEnum){
                    case Microsecond: piInfo.setText(stringResult(StopwatchTimeConverter.Microsecond)); break;
                    case Millisecond: piInfo.setText(stringResult(StopwatchTimeConverter.Millisecond)); break;
                    case Seconds: piInfo.setText(stringResult(StopwatchTimeConverter.Seconds)); break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                piInfo.setText(R.string.alu_stress_dropdown_notsel);
            }
        });



    }

    public void initializeTest() {
        this.scale = 5000;
        this.result = 0;
    }

    public void initializeTest(Long size) {
        scale = size.intValue();
        result = 0;
        piResult = new BigDecimal(0);
        initializeTest();
    }

    public void warmup() {
        int prevScale = scale;
        scale = 100;
        compute();
        scale = prevScale;
    }

    public void runTestWithTimer() {
        warmup();
        final StopwatchTimer timer = new StopwatchTimer();
        timer.startStopwatch();
        compute();
        result = timer.stopStopwatch();
    }

    public void compute() {
        shouldTestRun = true;
        MathContext context = new MathContext(this.scale);
        final ExecutorService threadPool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        final int chunkSize = TOTAL_ITERATIONS / THREAD_POOL_SIZE;

        for (int chunkStart = 0; chunkStart < TOTAL_ITERATIONS; chunkStart += chunkSize) {
            chunksResult.add(threadPool.submit(new ComputeChunk(chunkStart, Math.min(chunkStart + chunkSize, TOTAL_ITERATIONS), context)));
        }

        for (Future<BigDecimal> result : chunksResult) {
            if (!shouldTestRun) {
                break;
            }
            try {
                piResult = piResult.add(result.get(), context);
            } catch (Exception e) {
                if (shouldTestRun) {
                    Log.d(PIDigitsCalculation.class.getName(), e.getMessage());
                }
            }
        }

        shouldTestRun = false;
        threadPool.shutdownNow();
    }

    public void stopTest() {
        shouldTestRun = false;
        for (Future<BigDecimal> result : chunksResult) {
            result.cancel(true);
        }
    }

    public BigDecimal getDigitsOfPI() {
        return piResult;
    }

    private class ComputeChunk implements Callable<BigDecimal> {

        private int begin;
        private int end;
        private MathContext context;

        ComputeChunk(int begin, int end, MathContext context) {
            this.begin = begin;
            this.end = end;
            this.context = context;
        }

        @Override
        public BigDecimal call() {
            final BigDecimal powerBase = new BigDecimal(1.0 / 16);
            BigDecimal sigma = new BigDecimal(0);
            BigDecimal pwr = powerBase.pow(begin, context);
            BigDecimal sum;
            final BigDecimal[] terms = new BigDecimal[4];
            terms[0] = new BigDecimal(4);
            terms[1] = new BigDecimal(2);
            terms[2] = new BigDecimal(1);
            terms[3] = new BigDecimal(1);

            for (int i = begin; i < end && shouldTestRun; i++) {
                sum = terms[0].divide(BigDecimal.valueOf(8 * i + 1), context);
                sum = sum.subtract(terms[1].divide(BigDecimal.valueOf(8 * i + 4), context), context);
                sum = sum.subtract(terms[2].divide(BigDecimal.valueOf(8 * i + 5), context), context);
                sum = sum.subtract(terms[3].divide(BigDecimal.valueOf(8 * i + 6), context), context);
                sigma = sigma.add(pwr.multiply(sum, context), context);
                pwr = pwr.multiply(powerBase, context);
            }

            return sigma;
        }
    }

    public String stringResult(StopwatchTimeConverter type){
        switch (type){
            case Millisecond: return "Arithmetic operations done in: " + StopwatchTimeConverter.convertTime(result,StopwatchTimeConverter.Millisecond) + "ms";
            case Microsecond: return "Arithmetic operations done in: " + StopwatchTimeConverter.convertTime(result,StopwatchTimeConverter.Microsecond) + "us";
            case Seconds: return "Arithmetic operations done in: " + StopwatchTimeConverter.convertTime(result,StopwatchTimeConverter.Seconds) + "s";
            default: return null;
        }
    }

}
