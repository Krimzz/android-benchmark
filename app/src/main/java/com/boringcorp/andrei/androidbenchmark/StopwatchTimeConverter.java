package com.boringcorp.andrei.androidbenchmark;

public enum StopwatchTimeConverter {
    Microsecond,
    Millisecond,
    Seconds;

    public static long convertTime(long nanoSeconds,StopwatchTimeConverter type){
        switch (type){
            case Seconds: return nanoSeconds / 1000000000; // 10^9
            case Millisecond: return nanoSeconds / 1000000; // 10^6
            case Microsecond: return nanoSeconds / 1000; // 10^3
            default: return nanoSeconds;
        }
    }
}
